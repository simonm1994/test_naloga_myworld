﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Test_naloga_myWorld.Data;
using Test_naloga_myWorld.Dtos;
using Test_naloga_myWorld.Helpers;
using Test_naloga_myWorld.Models;

namespace Test_naloga_myWorld.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBookStore _repo;
        private readonly IMapper _mapper;

        public BooksController(IBookStore repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get(int page, int pageSize)
        {
            var books = await _repo.Get(page, pageSize);

            Response.AddPagination(books.CurrentPage, books.PageSize, books.TotalCount, books.TotalPages);

            return Ok(books);
        }

        [HttpPost]
        public async Task<IActionResult> Post(BookForSaveDto bookForSaveDto)
        {
            var book = _mapper.Map<BookForSaveDto, Book>(bookForSaveDto);

            var result = await _repo.Add(book);

            return Ok(result);
        }



    }
}