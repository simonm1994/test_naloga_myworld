﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test_naloga_myWorld.Helpers;
using Test_naloga_myWorld.Models;

namespace Test_naloga_myWorld.Data
{
    public interface IBookStore
    {
        Task<PagedList<Book>> Get(int page, int pageSize);
        //PagedList<Book> Get();
        Task<bool> Add(Book book);
    }
}
