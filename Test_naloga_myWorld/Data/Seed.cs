using System.Collections.Generic;
using Newtonsoft.Json;
using Test_naloga_myWorld.Models;

namespace Test_naloga_myWorld.Data
{
    public class DeserializeBook
    {
        public string title { get; set; }
        public string[] authors { get; set; }
        public string[] categories { get; set; }
        public string longDescription { get; set; }
        public string thumbnailUrl { get; set; }
    }

    public class Seed
    {
        private readonly DataContext _context;
        public Seed(DataContext context)
        {
            _context = context;
        }

        public void SeedBooks()
        {
            var booksData = System.IO.File.ReadAllText("Data/BookSeedData.json");
            var books = JsonConvert.DeserializeObject<List<DeserializeBook>>(booksData);

            foreach (var book in books)
            {
                var newBook = new Book
                {
                    Title = book.title,
                    SubTitle = book.categories[0],
                    Description = book.longDescription,
                    Author = book.authors[0],
                    ImageUrl = book.thumbnailUrl                
                };

                _context.Books.Add(newBook);

            }

            _context.SaveChanges();

        }
    }
}