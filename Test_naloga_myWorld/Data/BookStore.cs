﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Test_naloga_myWorld.Helpers;
using Test_naloga_myWorld.Models;

namespace Test_naloga_myWorld.Data
{
    public class BookStore : IBookStore
    {
        private readonly DataContext _context;

        public BookStore(DataContext context)
        {
            _context = context;
        }


        public async Task<bool> Add(Book book)
        {
            _context.Books.Add(book);

            return await _context.SaveChangesAsync() > 0;
        }

        //public async Task<List<Book>> Get(int page, int pageSize)
        public async Task<PagedList<Book>> Get(int page, int pageSize)
        {
            var books = _context.Books;

            return await PagedList<Book>.CreateAsync(books, page, pageSize);
        }
    }
}
