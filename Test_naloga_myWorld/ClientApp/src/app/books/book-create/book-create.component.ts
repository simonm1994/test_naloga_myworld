import { BookService } from './../../_services/book.service';
import { Book } from './../../_models/book';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.css']
})
export class BookCreateComponent implements OnInit {

  constructor(private bookService: BookService) { }

  ngOnInit() {
  }



  onSubmit(form: NgForm) {
    const book: Book = {
      id: null,
      title: form.value.title,
      subTitle: form.value.subTitle,
      description: form.value.description,
      author: form.value.author,
      imageUrl: form.value.imageUrl
    };

    this.bookService.addBook(book);

  }

}
