import { Pagination } from './../../_models/pagination';
import { BookService } from './../../_services/book.service';
import { Book } from './../../_models/book';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit, OnDestroy {
  books: Book[];
  pagination: Pagination = {
    totalItems: 0,
    totalPages: 0,
    currentPage: 1,
    itemsPerPage: 10
  };
  booksSubscription: Subscription;

  constructor(private bookService: BookService, private router: Router) { }

  ngOnInit() {
    this.loadBooks();
    this.booksSubscription = this.bookService.getBooksUpdatedListener()
      .subscribe((data) => {
        this.books = data.result;
        this.pagination = data.pagination;
      });


  }

  showDetail(id: number) {
    console.log('clicked');
    this.router.navigate(['books', id]);
  }

  loadBooks() {
    this.bookService.getBooks(this.pagination.currentPage, this.pagination.itemsPerPage);
  }

  pageChanged(event: any): void {
    this.pagination.currentPage = event.page;
    this.loadBooks();
  }

  ngOnDestroy() {
    this.booksSubscription.unsubscribe();
  }

}
