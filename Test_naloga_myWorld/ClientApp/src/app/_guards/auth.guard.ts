import { BookService } from './../_services/book.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor (private bookService: BookService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    //console.log(route);
    const id = +route.params['id'];
    if (id && id != 0) {
      if (this.bookService.getBook(id)) {
        return true;
      }
    }

    this.router.navigate(['/']);
    return false;
  }
}
