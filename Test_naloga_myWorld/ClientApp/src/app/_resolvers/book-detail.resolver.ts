import { BookService } from './../_services/book.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Book } from '../_models/book';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Injectable()
export class BookDetailResolver implements Resolve<Book> {

  constructor(private bookService: BookService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot) {
    console.log(this.bookService.getBook(route.params['id']));
    return this.bookService.getBook(route.params['id']);
  }

}
