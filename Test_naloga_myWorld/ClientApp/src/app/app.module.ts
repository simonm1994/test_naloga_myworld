import { BookDetailResolver } from './_resolvers/book-detail.resolver';
import { BookDetailComponent } from './books/book-detail/book-detail.component';
import { BookCreateComponent } from './books/book-create/book-create.component';
import { BookService } from './_services/book.service';
import { BookItemComponent } from './books/book-item/book-item.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { PaginationModule, TabsModule } from 'ngx-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookListComponent } from './books/book-list/book-list.component';
import { FormsModule } from '@angular/forms';

@NgModule({
   declarations: [
      AppComponent,
      BookListComponent,
      BookItemComponent,
      BookCreateComponent,
      BookDetailComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      FormsModule,
      PaginationModule.forRoot(),
      TabsModule.forRoot()
   ],
   providers: [
     BookService,
     BookDetailResolver
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
