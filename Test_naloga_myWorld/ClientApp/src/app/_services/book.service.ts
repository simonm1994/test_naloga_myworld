import { PaginatedResult } from './../_models/pagination';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Book } from '../_models/book';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class BookService {
  private books: Book[] = [];
  private booksUpdated = new Subject<PaginatedResult<Book[]>>();

  constructor(private http: HttpClient, private router: Router) { }


  getBooks(page?: number, pageSize?: number) {
    const paginatedResult: PaginatedResult<Book[]> = new PaginatedResult<Book[]>();

    let queryParams = ' ';
    if (page) {
      queryParams = `?page=${page}`;
    }
    this.http.get<Book[]>(environment.apiUrl + 'books' + queryParams, {observe: 'response'})
      .pipe(
        map(response => {
          paginatedResult.result = response.body;
          if (response.headers.get('Pagination') != null) {
            paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
          }
          return paginatedResult;
        })
      )
      .subscribe((data: PaginatedResult<Book[]>) => {

        this.books = data.result;
        this.booksUpdated.next(data);
      });
  }

  getBooksUpdatedListener() {
    return this.booksUpdated.asObservable();
  }

  getBook(id: number) {
    return this.books.find(b => b.id == id);
  }

  addBook(book: Book) {
    this.http.post(environment.apiUrl + 'books', book)
      .subscribe((response) => {
        this.router.navigate(['/']);
      });
  }

}
