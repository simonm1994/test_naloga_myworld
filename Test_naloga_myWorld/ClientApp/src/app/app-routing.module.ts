import { AuthGuard } from './_guards/auth.guard';
import { BookDetailResolver } from './_resolvers/book-detail.resolver';
import { BookDetailComponent } from './books/book-detail/book-detail.component';
import { BookCreateComponent } from './books/book-create/book-create.component';
import { BookListComponent } from './books/book-list/book-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'books', children: [
    {path: '', component: BookListComponent},
    {path: 'create', component: BookCreateComponent},
    {path: ':id', component: BookDetailComponent, canActivate: [AuthGuard], resolve: {book: BookDetailResolver}}
  ]},
  {path: '**', redirectTo: 'books', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
