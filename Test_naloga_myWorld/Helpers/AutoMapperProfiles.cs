﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Test_naloga_myWorld.Dtos;
using Test_naloga_myWorld.Models;

namespace Test_naloga_myWorld.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<BookForSaveDto, Book>();
        }
    }
}
